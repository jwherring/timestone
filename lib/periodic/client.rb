require_relative 'gateway'
require 'openssl'
require 'date'
require 'json'
require 'excon'


class PeriodicClient

  attr_reader :gateway

  def instantiate(apikey=nil, username=nil, endpoint=nil, protocol="http")
    if apikey && username && endpoint
      PeriodicGateway.instance.instantiate(apikey, username, endpoint, protocol)
    end
    @gateway = PeriodicGateway.instance
    self
  end

  @@instance = PeriodicClient.new

  def self.serialize_object(arg=nil)
    case arg.class.to_s
      when "Array"
        arg.join ','
      when "Hash"
        arg.sort.reduce('') { |a,el|
          "#{a}#{el.first}#{PeriodicClient.serialize_object(el.last)}"
        }
      else
        arg.to_s
    end
  end

  def self.instance
    @@instance
  end

  def add_auth_header(input, headers={})
    input = self.class.serialize_object input
    hmac = OpenSSL::HMAC.new @gateway.apikey, 'md5'
    hmac << input
    headers["WWW-Authenticate"] = "#{@gateway.username}::#{hmac.to_s}"
    headers
  end

  def add_json_header(headers={})
    headers["Content-type"] = "application/json"
    headers
  end

  def add_provider_header(headers={}, provider=nil)
    headers["x-periodic-provider"] = provider
    headers
  end

  def add_headers(input, provider=nil)
    headers = add_auth_header input
    if not provider.nil?
      headers = add_provider_header headers, provider
    end
    headers = add_json_header headers
  end

  def verify_hmac(input)
    body = JSON.generate input
    input = self.class.serialize_object input
    hmac = OpenSSL::HMAC.new @gateway.apikey, 'md5'
    hmac << input
    headers = {}
    headers["WWW-Authenticate"] = "#{@gateway.apikey}::#{hmac.to_s}"
    headers["Content-type"] = "application/json"
    url = "#{@gateway.protocol}://#{@gateway.baseendpoint}/verifyhmac"
    response = Excon.post url, :body => body, :headers => headers
  end

  def create(entitytype, body, provider=nil)
    extension = "#{entitytype}"
    urlbase = "#{@gateway.endpoint}/#{extension}"
    url = "#{@gateway.protocol}://#{urlbase}"
    Excon.new(url, :body => body.to_json, :headers => add_headers(body, provider)).post
  end

  def retrieve(entitytype, id, provider=nil)
    extension = "#{entitytype}/id/#{id}"
    rootpath = "/#{@gateway.mount}/#{extension}"
    urlbase = "#{@gateway.endpoint}/#{extension}"
    url = "#{@gateway.protocol}://#{urlbase}"
    Excon.new(url, :headers => add_headers(rootpath, provider)).get
  end

  def update(entitytype, id, body, provider=nil)
    extension = "#{entitytype}/id/#{id}"
    urlbase = "#{@gateway.endpoint}/#{extension}"
    url = "#{@gateway.protocol}://#{urlbase}"
    Excon.new(url, :body => body.to_json, :headers => add_headers(body, provider)).put
  end

  def destroy(entitytype, id, provider=nil)
    extension = "#{entitytype}/id/#{id}"
    rootpath = "/#{@gateway.mount}/#{extension}"
    urlbase = "#{@gateway.endpoint}/#{extension}"
    url = "#{@gateway.protocol}://#{urlbase}"
    Excon.new(url, :headers => add_headers(rootpath, provider)).delete
  end

  private_class_method :new

end
