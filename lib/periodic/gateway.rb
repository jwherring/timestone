class PeriodicGateway

  attr_reader :apikey, :username, :baseendpoint, :endpoint, :protocol, :mount

  def initialize()
    @apikey = nil
    @username = nil
    @endpoint = nil
    @baseendpoint = nil
    @protocol = "http"
    @mount = 'api'
  end

  def apikey=(apikey)
    @apikey ||= apikey
  end

  def username=(username)
    @username ||= username
  end

  def endpoint=(endpoint)
    @baseendpoint ||= endpoint
    @endpoint ||= "#{endpoint}/#{@mount}"
  end

  def protocol=(protocol)
    @protocol ||= protocol
  end

  def instantiate(apikey=nil, username=nil, endpoint=nil, protocol="http")
    @apikey ||= apikey
    @username ||= username
    @baseendpoint ||= endpoint
    @endpoint ||= "#{endpoint}/#{@mount}"
    @protocol ||= protocol
    self
  end

  @@instance = PeriodicGateway.new

  def self.instance
    @@instance
  end

  private_class_method :new

end
