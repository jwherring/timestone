require_relative 'client'

class PeriodicEntity

  attr_reader :client, :entitytype
  attr_accessor :body, :id, :provider, :status, :error

  def initialize(arg=nil, entitytype=nil)
    @provider = nil
    @status = 200
    @error = nil
    @body = {}
    @id = nil
    @client = PeriodicClient.instance
    @entitytype = entitytype
    case arg.class.to_s
      when "String"
        @id = arg
      when "Hash"
        @body = arg
    end
    @provider = @body["provider"] if @body.has_key?("provider")
  end

  def store_response response
    @status = response.status
    if @status == 200
      @error = nil
      @body = JSON.parse response.body
      if @body.has_key?("id")
        @id = @body["id"]
      end
    else
      @error = response.body
    end
  end

  def clear_on_success response
    @status = response.status
    if @status == 200
      @error = nil
      @body = {}
      @id = nil
      @provider = nil
    else
      @error = response.body
    end
  end

  def create(properties)
    store_response @client.create @entitytype, properties, @provider
  end

  def retrieve
    store_response @client.retrieve @entitytype, @id, @provider
  end

  def update(properties)
    store_response @client.update @entitytype, @id, properties, @provider
  end

  def destroy
    clear_on_success @client.destroy @entitytype, @id, @provider
  end

end

class ProviderEntity < PeriodicEntity

  def initialize(provider, arg=nil, entitytype=nil)
    mybody = {}
    case arg.class.to_s
      when "String"
        mybody[:id] = arg
      when "Hash"
        mybody.merge! arg
    end
    mybody["provider"] = provider
    super(mybody, entitytype)
  end

end
