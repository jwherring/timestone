require_relative 'entity'

class PeriodicMarketplace < PeriodicEntity

  def initialize(arg=nil)
    super(arg, "whitelabel")
  end

end

class PeriodicProvider < PeriodicEntity

  def initialize(arg=nil)
    super(arg, "provider")
  end

end

class PeriodicUser < PeriodicEntity

  def initialize(arg=nil)
    super(arg, "user")
  end

end

class PeriodicBookable < ProviderEntity

  def initialize(provider, arg=nil)
    super(provider, arg, "bookable")
  end

end

class PeriodicReservation < PeriodicEntity

  def initialize(arg=nil)
    super(arg, "reservation")
  end

end
