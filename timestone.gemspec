
Gem::Specification.new do |spec|
  spec.name           = 'timestone'
  spec.version        = '0.0.1'
  spec.authors        = ['Joshua Herring']
  spec.email          = 'josh@periodic.is'
  spec.summary        = 'Periodic.is API Ruby Gem'
  spec.description    = 'Ruby API interface to the periodic.is schedule anything API'
  spec.homepage       = 'http://periodic.is'
  spec.license        = 'MIT'

  spec.files          = Dir['lib/**/*rb']
  spec.require_paths  = ["lib"]

  spec.add_development_dependency 'bundler', '~> 1.10'
  spec.add_development_dependency 'rspec', '~> 3.3'
  spec.add_development_dependency 'rspec-core', '~> 3.3'
  spec.add_runtime_dependency 'json', '>= 1.7.7'
  spec.add_runtime_dependency 'excon', '>= 0.41.0'
end
