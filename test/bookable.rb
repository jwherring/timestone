require 'yaml'
require 'rspec'
require_relative '../lib/timestone'

RSpec.describe 'PeriodicBookable' do

  def initialize_bookable
    @configfile = File.join __dir__, 'config.yml'
    @config = YAML.load_file @configfile
    @endpoint = @config["endpoint"]
    @adminuser = @config["users"]["adminuser"]
    @providerbody = @config["providers"]["one"]
    @bookablebody = @config["bookables"]["one"]
    PeriodicClient.instance.instantiate @adminuser["apikey"], @adminuser["username"], @endpoint
    @provider = PeriodicProvider.new
    @bookable = PeriodicBookable.new @providerbody["subdomain"]
  end

  before(:context) do
    initialize_bookable()
  end

  it "bookable should have a client member of type PeriodicBookable" do
    expect(@bookable.client.class.to_s).to eq('PeriodicClient')
  end

  it "should be possible to get the apikey and username from the client gateway" do
    expect(@bookable.client.gateway.apikey).to eq(@adminuser["apikey"])
    expect(@bookable.client.gateway.username).to eq(@adminuser["username"])
  end

  it "bookable should identify its entitytype as bookable" do 
    expect(@bookable.entitytype).to eq("bookable")
  end

  it "the bookable should idenitify its provider as the provider subdomain passed in on creation" do
    expect(@bookable.provider).to eq(@providerbody["subdomain"])
  end

  context "After creating a provider and a bookable on that provider" do

    before(:context) do
      @provider.create @providerbody
      @bookable.create @bookablebody
    end

    after(:context) do
      @provider.destroy
    end

    it "the bookable should have an id of 32 alphanumeric characters" do
      expect(@bookable.id).to match(/[0-9a-zA-Z]{32}/)
    end

    it "the bookable should have 3 required questions" do
      expect(@bookable.body["questions"].length).to eq 3
    end

    it "the bookable should be available during its provider's businesshours" do
      expect(@bookable.body["availability"]).to eq @provider.body["businesshours"]
    end

    it "the bookable should be updatable" do
      @bookable.update({:name => "New Name"})
      expect(@bookable.status).to eq 200
      expect(@bookable.body["name"]).to eq "New Name"
    end

    it "after deleting the bookable, it should not be retrievable" do
      @bookable.destroy
      @provider.retrieve
      expect(@bookable.status).to eq 200
      expect(@bookable.body).to eq({}) 
      expect(@bookable.id).to eq nil
      expect(@provider.body["reservationtypes"].length).to eq 0
    end

  end

end

