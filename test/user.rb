require 'yaml'
require 'rspec'
require_relative '../lib/timestone'

RSpec.describe 'PeriodicUser' do

  def initialize_user
    @configfile = File.join __dir__, 'config.yml'
    @config = YAML.load_file @configfile
    @endpoint = @config["endpoint"]
    @adminuser = @config["users"]["adminuser"]
    @user = @config["users"]["provideruser"]
    PeriodicClient.instance.instantiate @adminuser["apikey"], @adminuser["username"], @endpoint
    @user = PeriodicUser.new
  end

  before(:context) do
    initialize_user()
  end

  it "user should have a client member of type PeriodicClient" do
    expect(@user.client.class.to_s).to eq('PeriodicClient')
  end

  it "should be possible to get the apikey and username from the client gateway" do
    expect(@user.client.gateway.apikey).to eq(@adminuser["apikey"])
    expect(@user.client.gateway.username).to eq(@adminuser["username"])
  end

  it "provider should identify its entitytype as user" do 
    expect(@user.entitytype).to eq("user")
  end

  context "after a provider has been created by an admin user" do

    before(:context) do
      @providerbody = @config["providers"]["one"]
      @userbody = @config["users"]["provideruser"]
      @provider = PeriodicProvider.new
      @provider.create(@providerbody)
      @userbody["providers"] = [@provider.body["subdomain"]]
      @userbody.delete "username"
      @uid = @provider.body["users"][0]
      @user = PeriodicUser.new @uid
      @user.retrieve()
      @provideruser = PeriodicUser.new
      @provideruser.create @userbody
      @anotheruser = PeriodicUser.new
    end

    after(:context) do
      @provider.destroy
    end

    it "the user should have a 32-character id of numbers and lowercase letters" do 
      expect(@provideruser.id).to match(/[a-z0-9]{32}/)
    end

    it "the user should have an api key of 32 alphnumeric characters" do
      expect(@provideruser.body["apikey"]).to match(/[a-zA-Z0-9]{32}/)
    end

    it "the user should have a slug equal to his email address" do
      expect(@provideruser.body["slug"]).to eq @provideruser.body["email"]
    end

    it "should not be possible to create a second user with the same slug" do
      @anotheruser.create @userbody
      expect(@anotheruser.status).to be 409
      expect(@anotheruser.error).to eq "\"Username or email is already in use\""
    end

    context "after updating the user" do

      before(:context) do
        @updates = {
          :firstname => "NewName"
        }
        @provideruser.update @updates
      end

      it "the user should show a status of 200 and reflect the update" do
        expect(@provideruser.status).to eq 200
        expect(@provideruser.body["firstname"]).to eq("NewName")
      end

    end

    context "after destroying the user" do

      before(:context) do
        @puid = @provideruser.id
        @provideruser.destroy()
      end

      it "the user should have status 200" do
        expect(@provideruser.status).to eq 200
      end

      it "the user should no longer have a body or an id" do
        expect(@provideruser.body).to eq({})
        expect(@provideruser.id).to eq(nil)
      end

      it "should no longer be possible to retrieve the associated user" do
        @testuser = PeriodicUser.new @puid
        @testuser.retrieve
        expect(@testuser.status).to eq 404
      end

    context "after creating two users" do

      before(:context) do
        @provideruser = PeriodicUser.new
        @provideruser.create @userbody
        @anotheruser = PeriodicUser.new
        @anotherbody = @config["users"]["employeeuser"]
        @anotherbody["providers"] = [@provider.body["subdomain"]]
        @anotheruser.create @anotherbody
      end

      it "both should exist, even though we're using an old email" do
        expect(@provideruser.status).to eq 200
        expect(@anotheruser.status).to eq 200
      end

      it "the user that specified a username should be assigned it as its slug" do
        expect(@anotheruser.body["slug"]).to eq @anotherbody["username"]
      end

      it "the user should not have a username property of its own" do
        expect(@anotheruser.body.has_key?("username")).to eq false
      end

      skip "calling index should return a list of both users" do
      end

    end

    end

  end

end

