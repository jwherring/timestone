require 'yaml'
require 'rspec'
require_relative '../lib/timestone'

RSpec.describe "PeriodicClient" do

  def load_test_user
    configfile = File.join __dir__, 'config.yml'
    config = YAML.load_file configfile
    @adminuser = config["users"]["adminuser"]
    @endpoint = config["endpoint"]
    PeriodicClient.instance.instantiate @adminuser["apikey"], @adminuser["username"], @endpoint
  end

  before(:example) do
    load_test_user
  end

  it "Instantiating the PeriodicClient with an apikey, username and endpoint should result in the creation of a member 'gateway' of type PeriodicGateway with the same apikey and username , and an endppint that has '/api' added to it" do
    expect(PeriodicClient.instance.gateway.apikey).to eq(@adminuser["apikey"])
    expect(PeriodicClient.instance.gateway.username).to eq(@adminuser["username"])
    expect(PeriodicClient.instance.gateway.endpoint).to eq("#{@endpoint}/api")
  end

  context "The PeriodicClient class is a singleton, so" do

    before(:example) do
      PeriodicClient.instance.instantiate "newkey", "newname", @endpoint
    end

    it "should not be possible to change the apikey and username once set by creating a new instance" do
      expect(PeriodicClient.instance.gateway.apikey).to eq(@adminuser["apikey"])
      expect(PeriodicClient.instance.gateway.username).to eq(@adminuser["username"])
    end

  end

end

