require 'yaml'
require 'rspec'
require_relative '../lib/timestone'


RSpec.describe "PeriodicMarketplace" do

  def initialize_marketplace
    configfile = File.join __dir__, 'config.yml'
    config = YAML.load_file configfile
    @endpoint = config["endpoint"]
    @superuser = config["users"]["superuser"]
    @adminuser = config["users"]["adminuser"]
    @marketplacebody = config["marketplaces"]["one"]
    PeriodicClient.instance.instantiate @adminuser["apikey"], @adminuser["username"], @endpoint
    @marketplace = PeriodicMarketplace.new
  end

  before(:example) do 
    initialize_marketplace()
  end

  it "should identify itself as PeriodicMarketplace class" do
    expect(@marketplace.class.to_s).to eq('PeriodicMarketplace')
  end

  it "should have a client member of type PeriodicClient" do
    expect(@marketplace.client.class.to_s).to eq('PeriodicClient')
  end

  it "should be possible to get the apikey and username from the client gateway" do
    expect(@marketplace.client.gateway.apikey).to eq(@adminuser["apikey"])
    expect(@marketplace.client.gateway.username).to eq(@adminuser["username"])
  end

  context "after an attempt to create a marketplace with an admin user has failed" do

    before(:example) do 
      @marketplace.create(@marketplacebody)
    end

    it "the status should be 'unauthorized'" do
      expect(@marketplace.status).to eq(401)
    end

  end

end

RSpec.describe "SuperadminMarketplace" do

  def initialize_marketplace
    configfile = File.join __dir__, 'config.yml'
    config = YAML.load_file configfile
    @endpoint = config["endpoint"]
    @superuser = config["users"]["superuser"]
    @marketplacebody = config["marketplaces"]["one"]
    PeriodicClient.instance.instantiate @superuser["apikey"], @superuser["username"], @endpoint
    @marketplace = PeriodicMarketplace.new
  end

  before(:example) do 
    initialize_marketplace()
  end

  context "after an attempt to create a marketplace with an admin user has failed" do

    before(:example) do 
      @marketplace.create(@marketplacebody)
    end

    it "the status should be 'success'" do
      puts @marketplace.client.gateway.username
      expect(@marketplace.status).to eq(200)
    end

  end

end
