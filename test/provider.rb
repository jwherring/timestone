require 'yaml'
require 'rspec'
require_relative '../lib/timestone'

RSpec.describe 'PeriodicProvider' do

  def initialize_provider
    @configfile = File.join __dir__, 'config.yml'
    @config = YAML.load_file @configfile
    @endpoint = @config["endpoint"]
    @adminuser = @config["users"]["adminuser"]
    PeriodicClient.instance.instantiate @adminuser["apikey"], @adminuser["username"], @endpoint
    @provider = PeriodicProvider.new
  end

  before(:context) do
    initialize_provider()
  end

  it "provider should have a client member of type PeriodicClient" do
    expect(@provider.client.class.to_s).to eq('PeriodicClient')
  end

  it "should be possible to get the apikey and username from the client gateway" do
    expect(@provider.client.gateway.apikey).to eq(@adminuser["apikey"])
    expect(@provider.client.gateway.username).to eq(@adminuser["username"])
  end

  it "provider should identify its entitytype as provider" do 
    expect(@provider.entitytype).to eq("provider")
  end

  context "after an attempt to create a provider by an admin user" do

    before(:context) do
      @providerbody = @config["providers"]["one"]
      @anotherproviderbody = @config["providers"]["two"]
      @savesubdomain = @anotherproviderbody["subdomain"]
      @anotherproviderbody["subdomain"] = @providerbody["subdomain"]
      @provider = PeriodicProvider.new
      @provider.create(@providerbody)
      @anotherprovider = PeriodicProvider.new
      @uid = @provider.body["users"][0]
      @user = PeriodicUser.new @uid
      @user.retrieve()
    end

    it "the provider should have a 32-character id of numbers and lowercase letters" do 
      expect(@provider.id).to match(/[a-z0-9]{32}/)
    end

    it "the provider should have an associated user" do
      expect(@uid).to match(/[a-z0-9]{32}/)
    end

    it "should not be possible to create a second provider with the same subdomain" do
      @anotherprovider.create @anotherproviderbody
      expect(@anotherprovider.status).to be 409
      expect(@anotherprovider.error).to eq "\"Subdomain is already in use\""
    end

    it "the associated user should be retrievable and have role 'provider'" do
      expect(@user.body["providers"][0]).to eq(@provider.body["subdomain"])
    end

    context "after updating the provider" do

      before(:context) do
        @updates = {
          :name => "New Name"
        }
        @provider.update @updates
      end

      it "the provider should show a status of 200 and reflect the update" do
        expect(@provider.status).to eq 200
        expect(@provider.body["name"]).to eq("New Name")
      end

    end

    context "after destroying the provider" do

      before(:context) do
        @provider.destroy()
        @user.retrieve()
      end

      it "the provider should have status 200" do
        expect(@provider.status).to eq 200
      end

      it "the provider should no longer have a body or an id" do
        expect(@provider.body).to eq({})
        expect(@provider.id).to eq(nil)
      end

      it "should no longer be possible to retrieve the associated user" do
        expect(@user.status).to eq 404
      end

    end

  end

  context "after an attempt to create a provider with missing fields" do

    before(:context) do
      @anotherproviderbody = @config["providers"]["three"]
      @anotherproviderbody.delete "name"
      @anotherprovider = PeriodicProvider.new
      @anotherprovider.create @anotherproviderbody
    end

    it "the server should respond with an error that indicates that the input is malformed" do
      expect(@anotherprovider.status).to eq 400
      expect(@anotherprovider.error).to eq "\"Missing required property: name\""
    end

    it "the provider should not have a body or an id" do
      expect(@anotherprovider.body).to eq({})
      expect(@anotherprovider.id).to eq nil
    end

  end

end

